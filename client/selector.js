const d3 = require("d3");
const $ = require("jquery");

class Selector {

	constructor(data, title) {
		this.id = title.replace(' ', '-');
		this.title = title;
		this.element = $(`
			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<label class="input-group-text" for="${this.id}">${capitalize(this.title)}</label>
				</div>
				<select class="custom-select" id="${this.id}"></select>
			</div>
		`);
		this.addTo($("form#findmushroom"));
		this.setOptions(data);
	}

	getValue() {
		return $(`select#${this.id}`).val();
	}

	setOptions(data) {
		const select = $(`select#${this.id}`);
		let html = "<option value='unknown'>Unknown</option>";
		const options = d3.map(data, (d) => {
			return d[this.title];
		}).keys();
		options.forEach((option) => html += `<option value="${option}">${capitalize(option)}</option>`);
		select.html(html);
	}

	addTo(div) {
		this.element.appendTo(div);
	}

	static clear() {
		$("form#findmushroom").html("");
	}
}

function capitalize(word) {
	return word[0].toUpperCase() + word.substring(1).toLowerCase();
}

module.exports = Selector;