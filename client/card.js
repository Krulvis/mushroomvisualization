const d3 = require('d3');

class Card {

	constructor(col, key, element) {
		this.html = `
			<div class="card text-white bg-danger mb-3" style="max-width: 18rem;">
			  <div class="card-header">${col}</div>
			  <div class="card-body">
			    <h5 class="card-title">${key}</h5>
			    <!--<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>-->
			  </div>
			</div>
		`;
		this.addTo(element);
	}

	addTo(element) {
		element.html(element.html() + this.html);
	}
}

function createCards(cols, df, element) {
	element.html("");
	let data = [];
	cols.forEach((col) => {
		const keys = d3.map(df, function (d) {
			return d[col];
		}).keys();
		let d = {
			column: col,
			titles: []
		};
		keys.forEach((key) => {
			const selection = df.filter((d) => d[col] === key);
			const poisonous = selection.filter((d) => d["poisonous"] === "Poisonous");
			const poisonousPercentage = poisonous.length / selection.length * 100;
			if (selection.length > 1 && poisonousPercentage === 100) {
				d.titles.push(key);
			}
		});
		data.push(d);
	});
	data = data.filter((d) => d.titles.length > 0);
	// data = data.splice(0, 10);
	data.forEach((d) => new Card(d.column, d.titles.join(", "), element))
}

module.exports = createCards;