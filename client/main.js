const $ = require("jquery");
const d3 = require("d3");
const Doughnut = require("./charts/doughnut");
const Scatter = require("./charts/scatter");
const Bar = require("./charts/bar");
const createCards = require("./card");
const Selector = require("./selector");
let df, doughnut, scatter, bar;
let selectors = [];
let cards = [];
// ((df["population"] == "clustered") & (df["cap-color"] == "white")
// 	| (~df["odor"].isin(["almond", "anise","none"]))
// 	| (df["spore-print-color"] == "green")
// 	| ((df["odor"] == "none") & (df["stalk-surface-below-ring"] == "scaly") & (df["stalk-color-above-ring"] != "brown"))
// 	| ((df["habitat"] == "leaves") & (df["cap-color"] == "white")))

const columns = ["cap-color", "stalk-color-above-ring", "stalk-surface-below-ring", "habitat", "odor", "population", "spore-print-color"];
$.get("/api/data", function (result) {
	// console.log(result);
	df = d3.csvParse(result);
	doughnut = new Doughnut(df);
	scatter = new Scatter(columns, df);
	// bar = new Bar(columns, df);
	createCards(columns, df, $("div#cards"));
	columns.forEach((column) => selectors.push(new Selector(df, column, onClick)));
	$("<h3 id='result'></h3>").appendTo($("form#findmushroom"));
	$("select").on("change", function () {
		onClick(this);
	});
});

function onClick(e) {
	let selection = df;

	selectors.filter((s) => s.getValue() !== "unknown").forEach((s) => {
		selection = selection.filter((d) => d[s.id] === s.getValue());
	});
	console.log(selection);
	doughnut.update(selection);
	scatter.update(selection);
	createCards(columns, selection, $("div#cards"));
	// bar.update(selection);
	if (doughnut.total === 0) {
		// $("h3#result").html(`There are no mushrooms with set filters.`);
	} else if (doughnut.poisonousPercentage === 0) {
		// $("h3#result").html(`Congratulations, you can eat the mushroom.`);
	} else if (doughnut.poisonousPercentage === 100) {
		$("h3#result").html(`Watch out, do <b>NOT</b> eat that mushroom.`);
	} else {
		// $("h3#result").html(`${100 - doughnut.poisonousPercentage}% of the ${doughnut.total} mushrooms that match the description are edible.`);
		const index = columns.indexOf(e.id) + 1;
		if (index < columns.length) {
			// selectors.push(new Selector(selection, columns[index], onClick));
		}
	}
}



