require("chart.js");
require('chartjs-plugin-colorschemes');
const d3 = require('d3');
const $ = require("jquery");

class Scatter {

	constructor(cols, df) {
		this.cols = cols;
		this.ctx = $(".chart#scatter")[0].getContext("2d");
		this.chart = new Chart(this.ctx, {
			type: 'scatter',
			data: {
				datasets: this.getDatasets(df)
			},
			options: {
				legend: {
					display: false
				},
				title: {
					display: false,
					text: 'Which mushrooms should you be careful for!'
				},
				scales: {
					yAxes: [{
						scaleLabel: {
							display: true,
							labelString: 'Poisonous percentage'
						},
						ticks: {
							callback: function (tick, index, ticks) {
								return tick.toString() + "%";
							}
						}
					}],
					xAxes: [{
						scaleLabel: {
							display: true,
							labelString: 'Amount of mushroom types'
						}
					}]
				},
				layout: {
					padding: {
						bottom: 20
					}
				},
				tooltips: {
					callbacks: {
						title: (item, data) => {
							console.log(item);
							return data.datasets[item[0].datasetIndex].label || "WHA"
						},
						label: function (tooltipItem, data) {
							const label = data.datasets[tooltipItem.datasetIndex].label || '';
							const poisonous = data.datasets[tooltipItem.datasetIndex].data[0].y || 'NaN';
							const amount = data.datasets[tooltipItem.datasetIndex].data[0].x || 'NaN';
							let text = [];
							text.push(`Amount of mushroom types: ${amount}`);
							text.push(`Poisonous: ${poisonous}%`);
							return text;
						}
					}
				}
			}
		});
	}

	getDatasets(df) {
		let datasets = [];
		this.cols.forEach((col) => {
			const keys = d3.map(df, function (d) {
				return d[col];
			}).keys();
			keys.forEach((key) => {
				const selection = df.filter((d) => d[col] === key);
				const poisonous = selection.filter((d) => d["poisonous"] === "Poisonous");
				const poisonousPercentage = poisonous.length / selection.length * 100;
				if (selection.length > 1) {
					datasets.push({
						label: `${col}: ${key}`,
						data: [{
							y: poisonousPercentage.toFixed(),
							x: selection.length
						}]
					});
					// console.log(`${col}: ${key} => ${selection.length}: ${poisonousPercentage.toFixed()}%`);
				}
			});
		});
		return datasets;
	}

	update(df) {
		this.chart.data.datasets = this.getDatasets(df);
		this.chart.update();
	}


}

module.exports = Scatter;