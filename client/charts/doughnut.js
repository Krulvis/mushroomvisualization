require("chart.js");
require('chartjs-plugin-colorschemes');
const $ = require("jquery");

Chart.pluginService.register({
	beforeDraw: function (chart) {
		if (chart.config.options.elements.center) {
			//Get ctx from string
			var ctx = chart.chart.ctx;

			//Get options from the center object in options
			var centerConfig = chart.config.options.elements.center;
			var fontStyle = centerConfig.fontStyle || 'Arial';
			var txt = centerConfig.text;
			var color = centerConfig.color || '#000';
			var sidePadding = centerConfig.sidePadding || 20;
			var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2);
			//Start with a base font of 30px
			ctx.font = "30px " + fontStyle;

			//Get the width of the string and also the width of the element minus 10 to give it 5px side padding
			var stringWidth = ctx.measureText(txt).width;
			var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

			// Find out how much the font can grow in width.
			var widthRatio = elementWidth / stringWidth;
			var newFontSize = Math.floor(30 * widthRatio);
			var elementHeight = (chart.innerRadius * 2);

			// Pick a new font size so it will not be larger than the height of label.
			var fontSizeToUse = Math.min(newFontSize, elementHeight);

			//Set font settings to draw it correctly.
			ctx.textAlign = 'center';
			ctx.textBaseline = 'middle';
			var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
			var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
			ctx.font = fontSizeToUse + "px " + fontStyle;
			ctx.fillStyle = color;

			//Draw text in center
			ctx.fillText(txt, centerX, centerY + 5);
			// ctx.fillText("Mushrooms:", centerX, centerY - 2);
			// ctx.font = "50px bold";
		}
	}
});


class Doughnut {

	constructor(data) {
		this.calculate(data);
		const ctx = $("#doughnut")[0].getContext("2d");
		this.chart = new Chart(ctx, {
			type: 'doughnut',
			data: {
				datasets: [this.getData(data)],
				labels: ["Edible", "Poisonous"],
			},
			options: {
				elements: {
					center: {
						text: this.getText(),
						color: '#00c000', // Default is #000000
						fontStyle: 'Arial', // Default is Arial
						sidePadding: 25 // Defualt is 20 (as a percentage)
					}
				}
			}
		});
		// ctx.fillText(`Mushrooms in selection: <br> ${total}`);
	}

	calculate(data) {
		this.total = data.length;
		this.poisonous = data.filter((d) => d["poisonous"] === "Poisonous");
		this.poisonousPercentage = (this.poisonous.length / this.total * 100).toFixed(0);
		$("h1#info").html(`The descriptions match ${this.total} mushroom types of which ${this.poisonous.length} are poisonous!`);
	}

	getData() {
		return {
			data: [100 - this.poisonousPercentage, this.poisonousPercentage],
			backgroundColor: ["green", "red"],
			// borderColor: ["#005b00", "#C30800"]
		}
	}

	getText() {
		return `${100 - this.poisonousPercentage}%`;
	}

	update(data) {
		this.calculate(data);
		this.chart.data.datasets = [this.getData()];
		this.chart.options.elements.center.text = this.getText();
		this.chart.update();
	}
}


module.exports = Doughnut;