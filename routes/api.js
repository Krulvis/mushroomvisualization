var express = require('express');
var path = require("path");
var router = express.Router();
var fs = require('fs');
const data = fs.readFileSync(path.join(__dirname, "../data/cleaned_mushroom_data.csv"), 'utf-8');

/* GET users listing. */
router.get('/data', function (req, res, next) {
	res.send(data);
});

module.exports = router;
